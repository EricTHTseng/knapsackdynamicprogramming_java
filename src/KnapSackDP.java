
public class KnapSackDP {

    public static void main(String[] arg) {
        int value[] = {10, 40, 30, 50};
        int weight[] = {5, 4, 6, 3};
        int W = 10;

        int result = knapsack(value, weight, W);
        System.out.println("Result: " + result);
    }

    private static int knapsack(int value[], int wt[], int W) {
        // Total number of items
        int N = value.length;

        int[][] matrix = new int[N + 1][W + 1];

        // Set matrix row 0 to 0 for all columns
        for(int column = 0; column <= W; column++) {
            matrix[0][column] = 0;
        }

        // Set matrix column 0 to 0 for all rows
        for(int row = 0; row <= N; row++) {
            matrix[row][0] = 0;
        }

        // Fill the matrix row by row
        for(int item = 1; item <= N; item++) {
            for(int weight = 1; weight <= W; weight++) {
                // If the current item weight is less than or equal to the running weight
                if(wt[item - 1] <= weight) {
                    matrix[item][weight] = Math.max(value[item - 1] + matrix[item - 1][weight - wt[item - 1]], matrix[item - 1][weight]);
                }
                else {
                    // If the current item weight is greater than the running weight, just carry the previous value without current item
                    matrix[item][weight] = matrix[item - 1][weight];
                }
            }
        }

        // Print the matrix
        for(int[] rows : matrix) {
            for(int column : rows) {
                System.out.format("%5d", column);
            }
            System.out.println();
        }

        return matrix[N][W];
    }
}
